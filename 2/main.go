package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type game struct {
	id    int
	valid bool
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	sum := 0

	cubeCounts := 0
	for scanner.Scan() {
		line := scanner.Text()
		gameParams := strings.Split(line, ":")
		id := getGameId(gameParams[0])
		if isValidGame(gameParams[1]) {
			sum += id
		}

		cubeCounts += getMaxCubeCounts(gameParams[1])
	}

	fmt.Println(sum)
	fmt.Println(cubeCounts)
}

func getGameId(name string) int {
	idStr := strings.Split(name, " ")[1]
	id, err := strconv.Atoi(idStr)
	if err != nil {
		log.Fatalf("can't convert %s to int", idStr)
	}
	return id
}

func getMaxCubeCounts(cubes string) int {
	draws := strings.Split(cubes, ";")

	maxRed := 0
	maxGreen := 0
	maxBlue := 0

	for _, draw := range draws {
		r, g, b := getCubeCounts(draw)

		if r > maxRed {
			maxRed = r
		}
		if g > maxGreen {
			maxGreen = g
		}
		if b > maxBlue {
			maxBlue = b
		}
	}
	return maxRed * maxGreen * maxBlue
}

func isValidGame(cubes string) bool {
	draws := strings.Split(cubes, ";")

	for _, draw := range draws {
		r, g, b := getCubeCounts(draw)

		if r > 12 || g > 13 || b > 14 {
			return false
		}
	}

	return true
}

func getCubeCounts(draw string) (red, green, blue int) {
	colours := strings.Split(draw, ", ")

	for _, colour := range colours {
		parts := strings.Split(strings.TrimSpace(colour), " ")
		amount := parts[0]
		c := parts[1]

		a, err := strconv.Atoi(amount)
		if err != nil {
			log.Fatalf("can't convert %s to int", amount)
		}

		if c == "red" {
			red = a
		} else if c == "green" {
			green = a
		} else if c == "blue" {
			blue = a
		}
	}

	return
}
