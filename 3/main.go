package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

type numberMatch struct {
	number    int
	lineIndex int
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var rows []string
	sum := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		rows = append(rows, scanner.Text())
	}

	for index, line := range rows {
		// get numbers on line
		r := regexp.MustCompile(`\d+`)
		matches := r.FindAllStringIndex(line, -1)
		fmt.Println(matches)
		for _, match := range matches {
			si := match[0]
			ei := match[1]
			num := line[si:ei]
			parsedNum, _ := strconv.Atoi(num)

			searchStartIndex := 0
			searchEndIndex := len(line)

			// if the start index is 0 then don't do anything
			if si != 0 {
				searchStartIndex = si - 1
				// search the string before
				before := line[searchStartIndex:si]
				if isSymbol(before) {
					fmt.Printf("%d is valid\n", parsedNum)
					// its valid
					sum += parsedNum
					continue
				}
			}

			// if the end index is at the end of the row, don't do anything
			if ei != searchEndIndex {
				searchEndIndex = ei + 1
				// search the string after
				after := line[ei:searchEndIndex]
				if isSymbol(after) {
					fmt.Printf("%d is valid\n", parsedNum)
					// its valid
					sum += parsedNum
					continue
				}
			}

			// fmt.Printf("search from %d to %d\n", searchStartIndex, searchEndIndex)
			// Search surrounding rows
			if index != 0 {
				// for first row we don't need to check above
				if isValidSurroundingRow(rows[index-1], searchStartIndex, searchEndIndex) {
					// its valid
					sum += parsedNum
					continue
				}
			}

			if index != (len(rows) - 1) {
				if isValidSurroundingRow(rows[index+1], searchStartIndex, searchEndIndex) {
					// its valid
					sum += parsedNum
					continue
				}
			}

		}
	}

	fmt.Println(sum)
}

func isSymbol(char string) bool {
	_, err := strconv.Atoi(char)
	return char != "." && err != nil
}

// Does the line contain a symbol
func isValidSurroundingRow(line string, si, ei int) bool {
	substr := line[si:ei]
	chars := []rune(substr)

	for _, char := range chars {
		if isSymbol(string(char)) {
			return true
		}
	}

	return false
}
