package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"unicode"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var allNums []int
	words := []string{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}

	for scanner.Scan() {
		line := scanner.Text()
		num := ""
		var wordBuf []string

		for _, char := range line {
			if unicode.IsDigit(char) {
				if len(num) == 0 {
					num = string(char)
				} else {
					num = string(num[0]) + string(char)
				}
				wordBuf = []string{}
			} else {
				// build up the word
				wordBuf = append(wordBuf, string(char))
				jw := strings.Join(wordBuf, "")
				fw := wordExists(jw, words)

				if fw != "" {
					n := mapStrToInt(fw)
					if len(num) == 0 {
						num = fmt.Sprintf("%d", n)
					} else {
						num = string(num[0]) + fmt.Sprintf("%d", n)
					}
				}
			}
		}
		if len(num) == 1 {
			num = string(num[0]) + string(num[0])
		}

		// fmt.Printf("%s = %s\n", line, num)

		val, err := strconv.Atoi(num)
		if err != nil {
			log.Fatal(err)
		}
		allNums = append(allNums, val)
	}

	total := 0
	for _, num := range allNums {
		total = total + num
	}

	fmt.Println(total)
}

func wordExists(word string, words []string) string {
	maxIndex := -1
	match := ""

	for _, w := range words {
		index := strings.LastIndex(word, w)
		if index > maxIndex {
			match = w
			maxIndex = index
		}
	}
	return match
}

func mapStrToInt(word string) int {
	switch word {
	case "one":
		return 1
	case "two":
		return 2
	case "three":
		return 3
	case "four":
		return 4
	case "five":
		return 5
	case "six":
		return 6
	case "seven":
		return 7
	case "eight":
		return 8
	case "nine":
		return 9
	}

	return 0
}
